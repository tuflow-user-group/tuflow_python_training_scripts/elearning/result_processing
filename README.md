These scripts are the completed examples and data from the Introduction to Python for TUFLOW Modellers eLearning course.  The course is split into two modules, data manipulation and results viewing and post processing.  This project contains the completed script examples for the data Results Viewing and Post Processing module. The Data Manipulation Examples can be found here (https://gitlab.com/tuflow-user-group/tuflow_python_training_scripts/elearning/data_manipulation).

The result_processing_01 script is developed incrementally to the final script result_processing_01d.py.  The intermittent stages are as follows:

- result_processing_01-Opening a TUFLOW Plot Control (TPC) File
- result_processing_01a-A function to handle errors
- result_processing_01b-Plotting a Time Series
- result_processing_01c-Plotting a Long Section Plot
- result_processing_01d-Stability Checking

The following script provides an example of processing and plotting TUFLOW map output from a netcdf file.
- result_processing_02-Post Processing Map Output

Challenge2_Solution, Challenge2_Solution_Extra_Credit provide solutions to the Challenge 2 provided challenges to test the understanding of the results viewing and post processing procedures introduced during the training.
