import pytuflow
import matplotlib.pyplot as plt


INPUT = 'data/plot/M03_5m_CLA_001.tpc'


def main():
    res = pytuflow.ResData()
    err, msg = res.load(INPUT)
    error_handler(err, msg)

    for channel in res.channels():
        err, msg, (x, y) = res.getTimeSeriesData(channel, 'V')
        error_handler(err, msg)
        fig, ax = plt.subplots()
        ax.plot(x, y, label=channel)
        ax.legend()
        plt.show()

    print('Finished')


def error_handler(err, msg):
    if err:
        exit(msg)


if __name__ == '__main__':
    main()