import pytuflow


INPUT = 'data/plot/M03_5m_CLA_001.tpc'


def main():
    res = pytuflow.ResData()
    err, msg = res.load(INPUT)
    if err:
        exit(msg)

    print('Finished')


if __name__ == '__main__':
    main()