import pytuflow
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


INPUT = 'data/plot/M03_5m_CLA_001.tpc'


def main():
    res = pytuflow.ResData()
    err, msg = res.load(INPUT)
    error_handler(err, msg)

    # for channel in res.channels():
    #     err, msg, (x, y) = res.getTimeSeriesData(channel, 'V')
    #     error_handler(err, msg)
    #     fig, ax = plt.subplots()
    #     ax.plot(x, y, label=channel)
    #     ax.legend()
    #     plt.show()

    for channel in res.channels():
        timestep = 'max'
        fig, ax = plt.subplots()
        # water level
        err, msg, (x, y) = res.getLongProfileData(timestep, 'h', channel)
        error_handler(err, msg)
        ax.plot(x, y, label='water level')
        # bed level
        err, msg, (x, y) = res.getLongProfileData(timestep, 'bed level', channel)
        error_handler(err, msg)
        ax.plot(x, y, label='bed level')
        # culverts
        pipes = res.getPipes()
        for pipe in pipes:
            polygon = Polygon(pipe, facecolor='0.9', edgecolor='0.5', label=channel)
            ax.add_patch(polygon)
        ax.legend()
        plt.show()

    print('Finished')


def error_handler(err, msg):
    if err:
        exit(msg)


if __name__ == '__main__':
    main()